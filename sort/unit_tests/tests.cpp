#include <gtest/gtest.h>

#include <deque>
#include <vector>
#include <string>
#include <algorithm>

#include "../bubble_sort.h"
#include "../insertion_sort.h"
#include "../selection_sort.h"
#include "../merge_sort.h"
#include "../quick_sort.h"

#include "../partition.h"

using namespace std;
using namespace in_a_nutshell;

//-------------------------------------------------------------------------------------------------

class TestSort : public ::testing::Test {
protected:
    deque<int> randDeqToSort;
    vector<double> randVectToSort;
    vector<string> emptyVectToSort;
    deque<float> randDeqToRevSort;
    vector<char> randVectToRevSort;
    string strToSort;

    virtual void SetUp() {
        for (size_t i = 0; i < 1000; ++i) {
            randDeqToSort.push_front(rand() - RAND_MAX / 2);
            randVectToSort.push_back(0.33 * rand());
            randDeqToRevSort.push_front(0.1 * rand() - RAND_MAX / 2);
            randVectToRevSort.push_back(rand() % 128);
            strToSort += char(rand() % 128);
        }
    }
};

TEST_F(TestSort, BubbleSort) {
    BubbleSort(randDeqToSort.begin(), randDeqToSort.end());
    BubbleSort(randVectToSort.begin(), randVectToSort.end());
    BubbleSort(emptyVectToSort.begin(), emptyVectToSort.end());
    BubbleSort(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>());
    BubbleSort(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>());
    BubbleSort(strToSort.begin(), strToSort.end());

    ASSERT_TRUE(is_sorted(randDeqToSort.begin(), randDeqToSort.end()));
    ASSERT_TRUE(is_sorted(randVectToSort.begin(), randVectToSort.end()));
    ASSERT_TRUE(is_sorted(emptyVectToSort.begin(), emptyVectToSort.end()));
    ASSERT_TRUE(is_sorted(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>()));
    ASSERT_TRUE(is_sorted(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>()));
    ASSERT_TRUE(is_sorted(strToSort.begin(), strToSort.end()));
}

TEST_F(TestSort, InsertionSort) {
    InsertionSort(randDeqToSort.begin(), randDeqToSort.end());
    InsertionSort(randVectToSort.begin(), randVectToSort.end());
    InsertionSort(emptyVectToSort.begin(), emptyVectToSort.end());
    InsertionSort(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>());
    InsertionSort(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>());
    InsertionSort(strToSort.begin(), strToSort.end());

    ASSERT_TRUE(is_sorted(randDeqToSort.begin(), randDeqToSort.end()));
    ASSERT_TRUE(is_sorted(randVectToSort.begin(), randVectToSort.end()));
    ASSERT_TRUE(is_sorted(emptyVectToSort.begin(), emptyVectToSort.end()));
    ASSERT_TRUE(is_sorted(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>()));
    ASSERT_TRUE(is_sorted(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>()));
    ASSERT_TRUE(is_sorted(strToSort.begin(), strToSort.end()));
}

TEST_F(TestSort, SelectionSort) {
    SelectionSort(randDeqToSort.begin(), randDeqToSort.end());
    SelectionSort(randVectToSort.begin(), randVectToSort.end());
    SelectionSort(emptyVectToSort.begin(), emptyVectToSort.end());
    SelectionSort(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>());
    SelectionSort(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>());
    SelectionSort(strToSort.begin(), strToSort.end());

    ASSERT_TRUE(is_sorted(randDeqToSort.begin(), randDeqToSort.end()));
    ASSERT_TRUE(is_sorted(randVectToSort.begin(), randVectToSort.end()));
    ASSERT_TRUE(is_sorted(emptyVectToSort.begin(), emptyVectToSort.end()));
    ASSERT_TRUE(is_sorted(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>()));
    ASSERT_TRUE(is_sorted(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>()));
    ASSERT_TRUE(is_sorted(strToSort.begin(), strToSort.end()));
}

TEST_F(TestSort, MergeSort) {
    MergeSort(randDeqToSort.begin(), randDeqToSort.end());
    MergeSort(randVectToSort.begin(), randVectToSort.end());
    MergeSort(emptyVectToSort.begin(), emptyVectToSort.end());
    MergeSort(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>());
    MergeSort(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>());
    MergeSort(strToSort.begin(), strToSort.end());

    ASSERT_TRUE(std::is_sorted(randDeqToSort.begin(), randDeqToSort.end()));
    ASSERT_TRUE(std::is_sorted(randVectToSort.begin(), randVectToSort.end()));
    ASSERT_TRUE(is_sorted(emptyVectToSort.begin(), emptyVectToSort.end()));
    ASSERT_TRUE(is_sorted(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>()));
    ASSERT_TRUE(is_sorted(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>()));
    ASSERT_TRUE(is_sorted(strToSort.begin(), strToSort.end()));
}

TEST_F(TestSort, QuickSort) {
    QuickSort(randDeqToSort.begin(), randDeqToSort.end());
    QuickSort(randVectToSort.begin(), randVectToSort.end());
    QuickSort(emptyVectToSort.begin(), emptyVectToSort.end());
    QuickSort(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>());
    QuickSort(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>());
    QuickSort(strToSort.begin(), strToSort.end());

    ASSERT_TRUE(std::is_sorted(randDeqToSort.begin(), randDeqToSort.end()));
    ASSERT_TRUE(std::is_sorted(randVectToSort.begin(), randVectToSort.end()));
    ASSERT_TRUE(is_sorted(emptyVectToSort.begin(), emptyVectToSort.end()));
    ASSERT_TRUE(is_sorted(randDeqToRevSort.begin(), randDeqToRevSort.end(), greater<float>()));
    ASSERT_TRUE(is_sorted(randVectToRevSort.begin(), randVectToRevSort.end(), greater<char>()));
    ASSERT_TRUE(is_sorted(strToSort.begin(), strToSort.end()));
}

//-------------------------------------------------------------------------------------------------


class TestPartition : public ::testing::Test {
protected:
    deque<double> deqToPartition;
    vector<int> vectToPartition;
    virtual void SetUp() {
        for (size_t i = 0; i < 100000; ++i) {
            deqToPartition.push_front(rand() / 2.71828);
            vectToPartition.push_back(rand() - RAND_MAX / 2);
        }
    }
};

TEST_F(TestPartition, Partition) {
    double dPivot = deqToPartition[rand() % deqToPartition.size()];
    int vPivot = vectToPartition[rand() % vectToPartition.size()];
    auto dIt = Partition(deqToPartition.begin(), deqToPartition.end(),
                        [dPivot](double el) { return el <= dPivot; });
    auto vIt = Partition(vectToPartition.begin(), vectToPartition.end(),
                        [vPivot](int el) { return el <= vPivot; });

    ASSERT_TRUE(all_of(deqToPartition.begin(), dIt, [dPivot](double el) { return el <= dPivot; }));
    ASSERT_TRUE(all_of(dIt, deqToPartition.end(), [dPivot](double el) { return el > dPivot; }));
    ASSERT_TRUE(all_of(vectToPartition.begin(), vIt, [vPivot](int el) { return el <= vPivot; }));
    ASSERT_TRUE(all_of(vIt, vectToPartition.end(), [vPivot](int el) { return el > vPivot; }));
}

TEST_F(TestPartition, LomutoPartition) {
    double dPivot = deqToPartition[rand() % deqToPartition.size()];
    int vPivot = vectToPartition[rand() % vectToPartition.size()];
    auto dIt = LomutoPartition(deqToPartition.begin(), deqToPartition.end(),
                              [dPivot](double el) { return el <= dPivot; });
    auto vIt = LomutoPartition(vectToPartition.begin(), vectToPartition.end(),
                              [vPivot](int el) { return el <= vPivot; });

    ASSERT_TRUE(all_of(deqToPartition.begin(), dIt, [dPivot](double el) { return el <= dPivot; }));
    ASSERT_TRUE(all_of(dIt, deqToPartition.end(), [dPivot](double el) { return el > dPivot; }));
    ASSERT_TRUE(all_of(vectToPartition.begin(), vIt, [vPivot](int el) { return el <= vPivot; }));
    ASSERT_TRUE(all_of(vIt, vectToPartition.end(), [vPivot](int el) { return el > vPivot; }));
}

//-------------------------------------------------------------------------------------------------

int main(int argc, char **argv) {
    testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}

//-------------------------------------------------------------------------------------------------
