#pragma once
#include <algorithm>

namespace in_a_nutshell {

template <typename RandomAccessIterator>
void SelectionSort(RandomAccessIterator first, RandomAccessIterator last) {
    for (RandomAccessIterator it = first; it < last; ++it) {
        std::swap(*it, *std::min_element(it, last));
    }
}

template <typename RandomAccessIterator, typename Compare>
void SelectionSort(RandomAccessIterator first, RandomAccessIterator last, Compare comp) {
    for (RandomAccessIterator it = first; it < last; ++it) {
        std::swap(*it, *std::min_element(it, last, comp));
    }
}

}
