#pragma once
#include <algorithm>

namespace in_a_nutshell {

template <typename RandomAccessIterator>
void InsertionSort(RandomAccessIterator first, RandomAccessIterator last) {
    for (RandomAccessIterator it = first; it < last; ++it) {
        std::for_each(std::upper_bound(first, it, *it), it,
                     [it](decltype(*first) element) { std::swap(element, *it); });
    }
}

template <typename RandomAccessIterator, typename Compare>
void InsertionSort(RandomAccessIterator first, RandomAccessIterator last, Compare comp) {
    for (RandomAccessIterator it = first; it < last; ++it) {
        std::for_each(std::upper_bound(first, it, *it, comp), it,
                     [it](decltype(*first) &element) { std::swap(element, *it); });
    }
}

}
