#pragma once
#include <algorithm>
#include <vector>

namespace in_a_nutshell {

template <typename RandomAccessIterator>
void MergeSort(RandomAccessIterator first, RandomAccessIterator last) {
    for (int blockLength = 1; blockLength < std::distance(first, last); blockLength *= 2) {
        for (RandomAccessIterator it = first; std::distance(it, last) > blockLength;
                                                                        it += 2 * blockLength) {
            RandomAccessIterator blockEndPos = std::distance(it, last) < 2 * blockLength ?
                                               last : it + 2 * blockLength;
            std::inplace_merge(it, it + blockLength, blockEndPos);
        }
    }
}

template <typename RandomAccessIterator, typename Compare>
void MergeSort(RandomAccessIterator first, RandomAccessIterator last, Compare comp) {
    for (int blockLength = 1; blockLength < std::distance(first, last); blockLength *= 2) {
        for (RandomAccessIterator it = first; std::distance(it, last) > blockLength;
                                                                        it += 2 * blockLength) {
            RandomAccessIterator blockEndPos = std::distance(it, last) < 2 * blockLength ?
                                               last : it + 2 * blockLength;
            std::inplace_merge(it, it + blockLength, blockEndPos, comp);
        }
    }
}

}
