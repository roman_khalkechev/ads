#pragma once
#include <algorithm>

namespace in_a_nutshell {

template <typename RandomAccessIterator>
void BubbleSort(RandomAccessIterator first, RandomAccessIterator last) {
    while(first != last--) {
        for (RandomAccessIterator it = first; it != last; ++it) {
            if (*std::next(it) < *it) {
                std::swap(*std::next(it), *it);
            }
        }
    }
}

template <typename RandomAccessIterator, typename Compare>
void BubbleSort(RandomAccessIterator first, RandomAccessIterator last, Compare comp) {
    while(first != last--) {
        for (RandomAccessIterator it = first; it != last; ++it) {
            if (comp(*std::next(it), *it)) {
                std::swap(*std::next(it), *it);
            }
        }
    }
}

}
