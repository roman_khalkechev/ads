#pragma once
#include <algorithm>
#include "partition.h"
#include "selection_sort.h"

namespace in_a_nutshell {

template <typename T, typename Compare>
struct TPivotComparator {
    T Pivot;
    Compare Comp;
    TPivotComparator(T pivot, Compare comp) : Pivot(pivot), Comp(comp) {}
    bool operator() (T element) {
        if (element == Pivot) {
            return rand() % 2 == 0 ? true : false;
        }
        return Comp(element, Pivot);
    }
};

template <typename RandomAccessIterator>
void QuickSort(RandomAccessIterator first, RandomAccessIterator last) {
start:
    if (std::distance(first, last) < 2) {
        return;
    } else if (std::distance(first, last) < 15) {
        return SelectionSort(first, last);
    }
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
    RandomAccessIterator pivotPos = first + rand() % std::distance(first, last);
    pivotPos = Partition(first, last, TPivotComparator<T, std::less<T> >(*pivotPos,
                                                                         std::less<T>()));
    // I use goto directive to save memory on the stack
    if (std::distance(first, pivotPos) < std::distance(pivotPos, last)) {
        QuickSort(first, pivotPos);
        first = pivotPos;
        goto start;
    } else {
        QuickSort(pivotPos, last);
        last = pivotPos;
        goto start;
    }
}

template <typename RandomAccessIterator, typename Compare>
void QuickSort(RandomAccessIterator first, RandomAccessIterator last, Compare comp) {
start:
    if (std::distance(first, last) < 2) {
        return;
    } else if (std::distance(first, last) < 15) {
        return SelectionSort(first, last, comp);
    }
    typedef typename std::iterator_traits<RandomAccessIterator>::value_type T;
    RandomAccessIterator pivotPos = first + rand() % std::distance(first, last);
    pivotPos = Partition(first, last, TPivotComparator<T, Compare>(*pivotPos, comp));
    // I use goto directive to save memory on the stack
    if (std::distance(first, pivotPos) < std::distance(pivotPos, last)) {
        QuickSort(first, pivotPos, comp);
        first = pivotPos;
        goto start;
    } else {
        QuickSort(pivotPos, last, comp);
        last = pivotPos;
        goto start;
    }
}

}
