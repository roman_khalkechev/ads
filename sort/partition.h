#pragma once
#include <algorithm>

namespace in_a_nutshell {

template <typename ForwardIterator, typename UnaryPredicate>
ForwardIterator Partition(ForwardIterator first, ForwardIterator last, UnaryPredicate pred) {
    while (first != last) {
        while (pred(*first)) {
            ++first;
            if (first == last) return first;
        }
        do {
            --last;
            if (first == last) return first;
        } while (!pred(*last));
        std::swap(*first, *last);
        ++first;
    }
    return first;
}

template <typename ForwardIterator, typename UnaryPredicate>
ForwardIterator LomutoPartition(ForwardIterator first, ForwardIterator last, UnaryPredicate pred) {
    ForwardIterator pivotPos = first;
    while (first != last) {
        if (pred(*first)) {
            std::swap(*pivotPos++, *first);
        }
        ++first;
    }
    return pivotPos;
}

}
